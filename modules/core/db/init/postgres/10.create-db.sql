-- begin TABLETBOT_TABLET
create table TABLETBOT_TABLET (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    primary key (ID)
)^
-- end TABLETBOT_TABLET
-- begin TABLETBOT_PHARMACOTHERAPEUTIC_GROUP
create table TABLETBOT_PHARMACOTHERAPEUTIC_GROUP (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(5096),
    --
    primary key (ID)
)^
-- end TABLETBOT_PHARMACOTHERAPEUTIC_GROUP
-- begin TABLETBOT_RELEASE_FORM
create table TABLETBOT_RELEASE_FORM (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(1024),
    --
    primary key (ID)
)^
-- end TABLETBOT_RELEASE_FORM
-- begin TABLETBOT_COUNTRY
create table TABLETBOT_COUNTRY (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(2048),
    --
    primary key (ID)
)^
-- end TABLETBOT_COUNTRY
-- begin TABLETBOT_EXPIRATION_DATE_TYPE
create table TABLETBOT_EXPIRATION_DATE_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(2048),
    --
    primary key (ID)
)^
-- end TABLETBOT_EXPIRATION_DATE_TYPE
-- begin TABLETBOT_DRUG_TYPE
create table TABLETBOT_DRUG_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(2048),
    --
    primary key (ID)
)^
-- end TABLETBOT_DRUG_TYPE
-- begin TABLETBOT_PREPARATION
create table TABLETBOT_PREPARATION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    ID_DRLZ varchar(64),
    TRADE_NAME varchar(2048),
    REALESE_FORM_ID uuid,
    VACATION_CONDITIONS varchar(2048),
    DRUG_COMPOSITION varchar(5096),
    PHARMACOTHERAPEUTIC_GROUP_ID uuid,
    MANUFACTURE_ID uuid,
    COUNTRY_ID uuid,
    ADDRESS_MANUFACTURE varchar(2048),
    REGISTRATION_CERTIFICATE_NUMBER varchar(1024),
    DRUG_TYPE_ID uuid,
    BIOLOGICAL_ORIGIN boolean,
    PLANT_ORIGIN boolean,
    ORPHAN boolean,
    HOMEOPATHIC boolean,
    INSTRUCTIONS_URL varchar(2048),
    EXPIRATION_DATE integer,
    EXPIRATION_DATE_TYPE_ID uuid,
    --
    primary key (ID)
)^
-- end TABLETBOT_PREPARATION
-- begin TABLETBOT_MANUFACTURE
create table TABLETBOT_MANUFACTURE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(2048),
    --
    primary key (ID)
)^
-- end TABLETBOT_MANUFACTURE
-- begin TABLETBOT_PREPARATION_PREPARATION_LINK
create table TABLETBOT_PREPARATION_PREPARATION_LINK (
    PREPARATION_1_ID uuid,
    PREPARATION_2_ID uuid,
    primary key (PREPARATION_1_ID, PREPARATION_2_ID)
)^
-- end TABLETBOT_PREPARATION_PREPARATION_LINK
-- begin TABLETBOT_CHOISE_USER
create table TABLETBOT_CHOISE_USER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    USERNAME varchar(2048),
    CHOISE varchar(50),
    --
    primary key (ID)
)^
-- end TABLETBOT_CHOISE_USER
