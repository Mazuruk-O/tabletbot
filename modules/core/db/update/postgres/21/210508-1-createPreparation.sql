create table TABLETBOT_PREPARATION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    ID_DRLZ varchar(64),
    TRADE_NAME varchar(2048),
    VACATION_CONDITIONS varchar(2048),
    DRUG_COMPOSITION varchar(5096),
    MANUFACTURE_ID uuid,
    COUNTRY_ID uuid,
    ADDRESS_MANUFACTURE varchar(2048),
    REGISTRATION_CERTIFICATE_NUMBER varchar(1024),
    DRUG_TYPE_ID uuid,
    BIOLOGICAL_ORIGIN boolean,
    PLANT_ORIGIN boolean,
    ORPHAN boolean,
    HOMEOPATHIC boolean,
    INSTRUCTIONS_URL varchar(2048),
    EXPIRATION_DATE integer,
    EXPIRATION_DATE_TYPE_ID uuid,
    --
    primary key (ID)
);