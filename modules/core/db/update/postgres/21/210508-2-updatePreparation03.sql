alter table TABLETBOT_PREPARATION add constraint FK_TABLETBOT_PREPARATION_ON_PHARMACOTHERAPEUTIC_GROUP foreign key (PHARMACOTHERAPEUTIC_GROUP_ID) references TABLETBOT_PHARMACOTHERAPEUTIC_GROUP(ID);
create index IDX_TABLETBOT_PREPARATION_ON_PHARMACOTHERAPEUTIC_GROUP on TABLETBOT_PREPARATION (PHARMACOTHERAPEUTIC_GROUP_ID);
