package ua.donnu.edu.tabletbot.core.config;

import com.haulmont.cuba.core.config.Config;
import com.haulmont.cuba.core.config.Property;
import com.haulmont.cuba.core.config.defaults.DefaultString;

public interface TabletBotConfig extends Config {

    @Property("bot.name")
    @DefaultString("tablet_from_bot")
    String getName();

    @Property("bot.token")
    @DefaultString("1864242592:AAGQ3ehkVacZXI-VYu-Fo36DFRc9qg7XxlM")
    String getToken();
}
