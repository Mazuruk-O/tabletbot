package ua.donnu.edu.tabletbot.service;

import com.haulmont.cuba.core.global.Events;
import com.haulmont.cuba.core.sys.events.AppContextInitializedEvent;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Service(BotInitializerService.NAME)
public class BotInitializerServiceBean implements BotInitializerService {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(BotInitializerServiceBean.class);

    @Autowired BotService bot;

    @Override
    @EventListener(AppContextInitializedEvent.class)
    @Order(Events.LOWEST_PLATFORM_PRECEDENCE + 100)
    public void Init() {
        try {
            log.info("Init bot!\n"+bot.getBotUsername()+"\n"+bot.getBotToken());
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(bot);
        } catch (TelegramApiException e) {
            log.error(e.getLocalizedMessage());
        }
    }
}