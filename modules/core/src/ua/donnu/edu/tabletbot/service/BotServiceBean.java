package ua.donnu.edu.tabletbot.service;

import com.haulmont.cuba.security.app.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ua.donnu.edu.tabletbot.core.config.TabletBotConfig;
import ua.donnu.edu.tabletbot.entity.MessageChoiseEnum;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service(BotService.NAME)
public class BotServiceBean extends TelegramLongPollingBot implements BotService {

    private static final Logger log = LoggerFactory.getLogger(BotServiceBean.class);
    private static final String START_COMMAND = "/start";
    private static final String HELP_COMMAND = "/help";
    private static final int START_PAGINATION = 0;

    @Inject private Authentication authentication;
    @Inject private TabletBotConfig tabletBotConfig;
    @Inject private MainMenuService mainMenuService;
    @Inject private UserChoiseService userChoiseService;
    @Inject private SearchMenuService searchMenuService;

    @Override
    public String getBotUsername() {
        return tabletBotConfig.getName();
    }

    @Override
    public String getBotToken() {
        return tabletBotConfig.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {
        authentication.begin();
        List<SendMessage> responseMessages = new ArrayList<>();
        String messageText = "";
        String chatId = "";
        String firstname = "";
        String lastname = "";
        String username = "";

        if(update.hasCallbackQuery()) {
            log.info("New event! " + update.getCallbackQuery().getFrom().getFirstName() + " " + update.getCallbackQuery().getFrom().getLastName() + " " + update.getCallbackQuery().getFrom().getUserName());
            if(Objects.nonNull(update.getCallbackQuery().getMessage())) {
                firstname = update.getCallbackQuery().getFrom().getFirstName();
                lastname = update.getCallbackQuery().getFrom().getLastName();
                username = update.getCallbackQuery().getFrom().getUserName();
                messageText = update.getCallbackQuery().getMessage().getText();
                chatId = update.getCallbackQuery().getMessage().getChatId().toString();
            }
        } else {
            log.info("New event! " + update.getMessage().getFrom().getFirstName() + " " + update.getMessage().getFrom().getLastName() + " " + update.getMessage().getFrom().getUserName());
            if (Objects.nonNull(update.getMessage())) {
                firstname = update.getMessage().getFrom().getFirstName();
                lastname = update.getMessage().getFrom().getLastName();
                username = update.getMessage().getFrom().getUserName();
                messageText = update.getMessage().getText();
                chatId = update.getMessage().getChatId().toString();
            }
        }

        if(update.hasCallbackQuery() && update.getCallbackQuery().getData().contains(";")) {
            messageText = update.getCallbackQuery().getData();
            MessageChoiseEnum choise = MessageChoiseEnum.fromId(messageText.split(";")[0]);
            int pagination = Integer.parseInt(messageText.split(";")[2]);
            String name = messageText.split(";")[1];

            switch (choise) {
                case     ANALOG:     searchMenuService.analogs(responseMessages, name, pagination, chatId);            break;
                case     SUBSTANCE:  searchMenuService.activeSubstance(responseMessages, name, pagination, chatId);    break;
                default: TRADE_NAME: searchMenuService.tradeName(responseMessages, name, pagination, chatId);          break;
            }
        } else if(messageText.contains(START_COMMAND)) {
            mainMenuService.main(responseMessages, chatId);
        } else if(messageText.contains(HELP_COMMAND)) {
            mainMenuService.help(responseMessages, chatId);
        } else {
            try {
                MessageChoiseEnum messageChoiseEnum = MessageChoiseEnum.fromId(messageText);
                switch (messageChoiseEnum) {
                    case MAIN:       mainMenuService.main(responseMessages, chatId);                         break;
                    case SEARCH:     mainMenuService.search(responseMessages, chatId);                       break;
                    case BACK:       mainMenuService.search(responseMessages, chatId);                       break;
                    case TRADE_NAME: searchMenuService.tradeName(update, responseMessages, chatId);          break;
                    case ANALOG:     searchMenuService.analogs(update, responseMessages, chatId);            break;
                    case SUBSTANCE:  searchMenuService.activeSubstance(update, responseMessages, chatId);    break;
                    default:         mainMenuService.main(responseMessages, chatId);                         break;
                }
            } catch (NullPointerException e) {
                MessageChoiseEnum lastChoise = userChoiseService.findLastChoise(firstname, lastname, username);
                switch (lastChoise) {
                    case TRADE_NAME: searchMenuService.tradeName(responseMessages, messageText, START_PAGINATION, chatId);       break;
                    case ANALOG:     searchMenuService.analogs(responseMessages, messageText, START_PAGINATION, chatId);         break;
                    case SUBSTANCE:  searchMenuService.activeSubstance(responseMessages, messageText, START_PAGINATION, chatId); break;
                    default:         mainMenuService.search(responseMessages, chatId);                                           break;
                }
            }
        }

        responseMessages.forEach(m -> {
            try {
                execute(m);
            } catch (TelegramApiException e) {
                log.debug(e.toString());
            }
        });

        authentication.end();
    }

    private SendMessage createMessage(String message, String chatId) {
        return SendMessage.builder()
                .chatId(chatId)
                .text(message)
                .build();
    }

    private void testInlineKeyboard2(Update update) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        inlineKeyboardButton.setText("Up!");
        inlineKeyboardButton.setCallbackData("update_msg_text");

        rowInline.add(inlineKeyboardButton);
        // Set the keyboard to the markup
        rowsInline.add(rowInline);
        // Add it to the message
        markupInline.setKeyboard(rowsInline);

        int message_id = update.getCallbackQuery().getMessage().getMessageId();
        long chat_id = update.getCallbackQuery().getMessage().getChatId();

        EditMessageText new_message = EditMessageText.builder()
                .chatId(Long.toString(chat_id))
                .messageId(message_id)
                .text("/")
                .replyMarkup(markupInline)
                .build();
        try {
            execute(new_message);
        } catch (TelegramApiException e) {
            log.debug(e.toString());
        }
    }

    private void testInlineKeyboard(SendMessage.SendMessageBuilder builder) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        inlineKeyboardButton.setText("Up!");
        inlineKeyboardButton.setCallbackData("update_msg_text");

        rowInline.add(inlineKeyboardButton);
        // Set the keyboard to the markup
        rowsInline.add(rowInline);
        // Add it to the message
        markupInline.setKeyboard(rowsInline);

        builder.text("//");
        SendMessage build = builder.build();
        build.setReplyMarkup(markupInline);

        try {
            execute(build);
        } catch (TelegramApiException e) {
            log.debug(e.toString());
        }
    }
}