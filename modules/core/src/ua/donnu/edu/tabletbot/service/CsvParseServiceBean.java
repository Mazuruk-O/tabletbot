package ua.donnu.edu.tabletbot.service;

import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.EntitySet;
import com.opencsv.CSVReader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ua.donnu.edu.tabletbot.entity.*;

import javax.inject.Inject;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;

@Service(CsvParseService.NAME)
public class CsvParseServiceBean implements CsvParseService {

    private static final Logger log = LoggerFactory.getLogger(CsvParseServiceBean.class);
    private static final Preparation NOT_FIND = new Preparation();

    @Inject private DataManager dataManager;

    static String query = "https://tabletki.ua/uk/search/?q=";

    public static void main(String[] args) {
        try {
            Document doc0 = Jsoup.connect(query + "АДВЕЙТ".toLowerCase()).get();
            String link = doc0.select("div.good-block").select("a").first().attr("href");

            Document doc = Jsoup.connect(link + "analogi/").get();
            Elements newsHeadlines = doc.select("div.analogi-item-goods");

            for (Element headline : newsHeadlines) {
                Elements a = headline.select("a");
                String title = a.attr("title");
                String s = title.split(" ")[0];
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Init2() {
        List<Preparation> preparations = dataManager.load(Preparation.class).view("preparation-view").list();
        int i = 0;

        for(Preparation p: preparations) {
            System.out.println(i);
            i++;
            Document doc0 = null;
            try {
                try {
                    doc0 = Jsoup.connect(query + p.getTradeName().toLowerCase()).get();
                    String link = doc0.select("div.good-block").select("a").first().attr("href");

                    Document doc = Jsoup.connect(link + "analogi/").get();
                    Elements newsHeadlines = doc.select("div.analogi-item-goods");

                    List<Preparation> analogs = new ArrayList<>();

                    for (Element headline : newsHeadlines) {
                        String analog = headline.select("a").attr("title").split(" ")[0];
                        Preparation analogEntity = findPreparationAnalog(analog, preparations);
                        if(!NOT_FIND.equals(analogEntity)) {
                            analogs.add(analogEntity);
                        }
                    }

                    p.setAnalogs(analogs);
                } catch (NullPointerException e) {

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        CommitContext commitContext = new CommitContext(preparations);
        EntitySet commit = dataManager.commit(commitContext);
        System.out.println("SAVED");
    }

    private Preparation findPreparationAnalog(String analog, List<Preparation> preparations) {
        for (Preparation preparation : preparations) {
            if(preparation.getTradeName().equalsIgnoreCase(analog)) {
                return preparation;
            }
        }

        return NOT_FIND;
    }

    @Override
    public void Init() {
        String file = "C:\\Users\\Oleg.Mazuruk\\Project\\Java\\test\\tabletbot\\reestr.csv";
        try(FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("Windows-1251"));
            CSVReader reader = new CSVReader(isr, ';')) {

            List<Preparation> preparations = new ArrayList<>();
            Set<ReleaseForm> releaseForms = new HashSet<>();
            Set<Manufacture> manufactures = new HashSet<>();
            Set<Country> countries = new HashSet<>();
            Set<DrugType> drugTypes = new HashSet<>();
            Set<PharmacotherapeuticGroup> pharmacotherapeuticGroups = new HashSet<>();
            Set<ExpirationDateType> expirationDateTypes = new HashSet<>();

            String line[] = reader.readNext(); // skip first line(column names)
            while ((line = reader.readNext()) != null) {
                Preparation preparation = dataManager.create(Preparation.class);
                preparation.setIdDRLZ(line[0]);
                preparation.setTradeName(line[1]);
                preparation.setVacationConditions(line[2]);
                setRealseForm(line[3], preparation, releaseForms);
                preparation.setDrugComposition(line[4]);
                setManufacture(line[6], preparation, manufactures);
                setCountry(line[7], preparation, countries);
                preparation.setAddressManufacture(line[8]);
                preparation.setRegistrationCertificateNumber(line[9]);
                setDrugType(line[10], preparation, drugTypes);
                setBiologicalOrigin(line[11], preparation);
                setPlantOrigin(line[12], preparation);
                setOrphan(line[13], preparation);
                setHomeopathic(line[14], preparation);
                preparation.setInstructionsURL(line[15]);
                try {
                    preparation.setExpirationDate(Integer.parseInt(line[16]));
                    setExpirationDateType(line[17], preparation, expirationDateTypes);
                } catch (NumberFormatException e) {
                    preparation.setExpirationDate(0);
                    setExpirationDateType("місяць", preparation, expirationDateTypes);
                }

                setPharmacodynamicGroup(line[5], preparation, pharmacotherapeuticGroups);
                System.out.println(preparation);

                preparations.add(preparation);
            }

            CommitContext commitContext = new CommitContext(releaseForms);
            EntitySet commit = dataManager.commit(commitContext);

            CommitContext commitContext2 = new CommitContext(manufactures);
            EntitySet commit2 = dataManager.commit(commitContext2);

            CommitContext commitContext3 = new CommitContext(countries);
            EntitySet commit3 = dataManager.commit(commitContext3);

            CommitContext commitContext4 = new CommitContext(drugTypes);
            EntitySet commit4 = dataManager.commit(commitContext4);

            CommitContext commitContext5 = new CommitContext(pharmacotherapeuticGroups);
            EntitySet commit5 = dataManager.commit(commitContext5);

            CommitContext commitContext6 = new CommitContext(expirationDateTypes);
            EntitySet commit6 = dataManager.commit(commitContext6);

            CommitContext commitContext7 = new CommitContext(preparations);
            EntitySet commit7 = dataManager.commit(commitContext7);


            System.out.println("Preparation size:" + preparations.size());
            System.out.println("manufactures size:" + manufactures.size());
            System.out.println("countries size:" + manufactures.size());
            System.out.println("drugTypes size:" + drugTypes.size());
            System.out.println("Saved!!!!!!");


        } catch (IOException exc) {
            log.warn("Parse file Exception! Try again!");
        }
    }

    private void setPharmacodynamicGroup(String s, Preparation preparation, Set<PharmacotherapeuticGroup> pharmacotherapeuticGroups) {
        if(pharmacotherapeuticGroups.isEmpty()) {
            PharmacotherapeuticGroup entity = dataManager.create(PharmacotherapeuticGroup.class);
            entity.setName(s);
            pharmacotherapeuticGroups.add(entity);
            preparation.setPharmacotherapeuticGroup(entity);
        } else {
            pharmacotherapeuticGroups.forEach(e -> {
                if(e.getName().equalsIgnoreCase(s)) {
                    preparation.setPharmacotherapeuticGroup(e);
                }
            });

            if(Objects.isNull(preparation.getPharmacotherapeuticGroup())) {
                PharmacotherapeuticGroup entity = dataManager.create(PharmacotherapeuticGroup.class);
                entity.setName(s);
                pharmacotherapeuticGroups.add(entity);
                preparation.setPharmacotherapeuticGroup(entity);
            }
        }
    }

    private void setRealseForm(String s, Preparation preparation, Set<ReleaseForm> releaseForms) {
        if(releaseForms.isEmpty()) {
            ReleaseForm entity = dataManager.create(ReleaseForm.class);
            entity.setName(s);
            releaseForms.add(entity);
            preparation.setRealeseForm(entity);
        } else {
            releaseForms.forEach(e -> {
                if(e.getName().equalsIgnoreCase(s)) {
                    preparation.setRealeseForm(e);
                }
            });

            if(Objects.isNull(preparation.getRealeseForm())) {
                ReleaseForm entity = dataManager.create(ReleaseForm.class);
                entity.setName(s);
                releaseForms.add(entity);
                preparation.setRealeseForm(entity);
            }
        }
    }

    private void setExpirationDateType(String s, Preparation preparation, Set<ExpirationDateType> expirationDateTypes) {
        if(expirationDateTypes.isEmpty()) {
            ExpirationDateType entity = dataManager.create(ExpirationDateType.class);
            entity.setName(s);
            expirationDateTypes.add(entity);
            preparation.setExpirationDateType(entity);
        } else {
            expirationDateTypes.forEach(e -> {
                if(e.getName().equalsIgnoreCase(s)) {
                    preparation.setExpirationDateType(e);
                }
            });

            if(Objects.isNull(preparation.getExpirationDateType())) {
                ExpirationDateType entity = dataManager.create(ExpirationDateType.class);
                entity.setName(s);
                expirationDateTypes.add(entity);
                preparation.setExpirationDateType(entity);
            }
        }
    }

    private void setHomeopathic(String s, Preparation preparation) {
        if(YES.equalsIgnoreCase(s)) {
            preparation.setHomeopathic(true);
        } else {
            preparation.setHomeopathic(false);
        }
    }

    private void setOrphan(String s, Preparation preparation) {
        if(YES.equalsIgnoreCase(s)) {
            preparation.setOrphan(true);
        } else {
            preparation.setOrphan(false);
        }
    }

    private void setPlantOrigin(String s, Preparation preparation) {
        if(YES.equalsIgnoreCase(s)) {
            preparation.setPlantOrigin(true);
        } else {
            preparation.setPlantOrigin(false);
        }
    }

    private void setBiologicalOrigin(String s, Preparation preparation) {
        if(YES.equalsIgnoreCase(s)) {
            preparation.setBiologicalOrigin(true);
        } else {
            preparation.setBiologicalOrigin(false);
        }
    }

    private static final String YES = "Так";

    private void setDrugType(String s, Preparation preparation, Set<DrugType> drugTypes) {
        if(drugTypes.isEmpty()) {
            DrugType entity = dataManager.create(DrugType.class);
            entity.setName(s);
            drugTypes.add(entity);
            preparation.setDrugType(entity);
        } else {
            drugTypes.forEach(e -> {
                if(e.getName().equalsIgnoreCase(s)) {
                    preparation.setDrugType(e);
                }
            });

            if(Objects.isNull(preparation.getDrugType())) {
                DrugType entity = dataManager.create(DrugType.class);
                entity.setName(s);
                drugTypes.add(entity);
                preparation.setDrugType(entity);
            }
        }
    }

    private void setCountry(String s, Preparation preparation, Set<Country> countries) {
        if(countries.isEmpty()) {
            Country entity = dataManager.create(Country.class);
            entity.setName(s);
            countries.add(entity);
            preparation.setCountry(entity);
        } else {
            countries.forEach(e -> {
                if(e.getName().equalsIgnoreCase(s)) {
                    preparation.setCountry(e);
                }
            });

            if(Objects.isNull(preparation.getCountry())) {
                Country entity = dataManager.create(Country.class);
                entity.setName(s);
                countries.add(entity);
                preparation.setCountry(entity);
            }
        }
    }

    private void setManufacture(String s, Preparation preparation, Set<Manufacture> manufactures) {
        if(manufactures.isEmpty()) {
            Manufacture manufacture = dataManager.create(Manufacture.class);
            manufacture.setName(s);
            manufactures.add(manufacture);
            preparation.setManufacture(manufacture);
        } else {
            manufactures.forEach(m -> {
                if(m.getName().equalsIgnoreCase(s)) {
                    preparation.setManufacture(m);
                }
            });

            if(Objects.isNull(preparation.getManufacture())) {
                Manufacture manufacture = dataManager.create(Manufacture.class);
                manufacture.setName(s);
                manufactures.add(manufacture);
                preparation.setManufacture(manufacture);
            }
        }
    }
}