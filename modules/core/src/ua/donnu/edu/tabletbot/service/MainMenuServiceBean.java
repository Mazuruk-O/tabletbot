package ua.donnu.edu.tabletbot.service;

import com.haulmont.cuba.core.sys.events.AppContextInitializedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.donnu.edu.tabletbot.entity.MessageChoiseEnum;

import java.util.Arrays;
import java.util.List;

@Service(MainMenuService.NAME)
public class MainMenuServiceBean implements MainMenuService {

    private ReplyKeyboardMarkup mainKeyboardMarkup;
    private ReplyKeyboardMarkup searchKeyboardMarkup;

    @Override
    public List<SendMessage> main(List<SendMessage> responseMessages, String chatId) {
        SendMessage message = SendMessage.builder()
                .text("Ви у головному меню")
                .chatId(chatId)
                .replyMarkup(mainKeyboardMarkup)
                .build();

        responseMessages.add(message);

        return responseMessages;
    }

    @Override
    public List<SendMessage> search(List<SendMessage> responseMessages, String chatId) {
        SendMessage message = SendMessage.builder()
                .text("Спробуємо відшукати необхідний препарат?")
                .chatId(chatId)
                .replyMarkup(searchKeyboardMarkup)
                .build();

        responseMessages.add(message);

        return responseMessages;
    }

    @Override
    public List<SendMessage> help(List<SendMessage> responseMessages, String chatId) {
        SendMessage message = SendMessage.builder()
                .text("У вас є можливість відшукати необхідний препарат за назвою, діючою речовиною або підібрати аналог.\n\n" +
                        "Для того щоб здійснити пошук препарату необхідно натиснути кнопку \"Пошук\" у головному меню " +
                        "та обрати необхідну вам категорію.")
                .chatId(chatId)
                .replyMarkup(mainKeyboardMarkup)
                .build();

        responseMessages.add(message);

        return responseMessages;
    }

    @Override
    @EventListener(AppContextInitializedEvent.class)
    public void Init() {
        KeyboardRow searchKeyboard = new KeyboardRow();
        searchKeyboard.add(MessageChoiseEnum.SEARCH.getId());

        this.mainKeyboardMarkup = ReplyKeyboardMarkup.builder()
                .resizeKeyboard(true)
                .oneTimeKeyboard(false)
                .keyboard(Arrays.asList(searchKeyboard))
                .build();

        KeyboardRow analogKeyboard = new KeyboardRow();
        analogKeyboard.add(MessageChoiseEnum.ANALOG.getId());

        KeyboardRow substanceKeyboard = new KeyboardRow();
        substanceKeyboard.add(MessageChoiseEnum.SUBSTANCE.getId());

        KeyboardRow tradeNameKeyboard = new KeyboardRow();
        tradeNameKeyboard.add(MessageChoiseEnum.TRADE_NAME.getId());

        KeyboardRow backKeyboard = new KeyboardRow();
        backKeyboard.add(MessageChoiseEnum.MAIN.getId());

        this.searchKeyboardMarkup = ReplyKeyboardMarkup.builder()
                .resizeKeyboard(true)
                .oneTimeKeyboard(false)
                .keyboard(Arrays.asList(tradeNameKeyboard, analogKeyboard, substanceKeyboard, backKeyboard))
                .build();
    }
}