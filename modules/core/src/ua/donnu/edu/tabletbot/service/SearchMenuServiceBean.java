package ua.donnu.edu.tabletbot.service;

import com.haulmont.cuba.core.sys.events.AppContextInitializedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import ua.donnu.edu.tabletbot.entity.MessageChoiseEnum;
import ua.donnu.edu.tabletbot.entity.Preparation;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service(SearchMenuService.NAME)
public class SearchMenuServiceBean implements SearchMenuService {

    private static final Logger log = LoggerFactory.getLogger(SearchMenuServiceBean.class);

    private static final int  ONE_ELEMENT  = 1  ;
    private static final int  FIRST_ELEM   = 0  ;
    private static final int START_LIST    = 0  ;
    private static final int START_PAGINATION    = 0  ;

    private ReplyKeyboardMarkup backToMainMenuKeyboardMarkup;

    @Inject private SearchService searchService;
    @Inject private UserChoiseService userChoiseService;

    @Override
    @EventListener(AppContextInitializedEvent.class)
    public void Init() {
        KeyboardRow backToMainMenuKeyboardMarkup = new KeyboardRow();
        backToMainMenuKeyboardMarkup.add(MessageChoiseEnum.BACK.getId());

        this.backToMainMenuKeyboardMarkup = ReplyKeyboardMarkup.builder()
                .resizeKeyboard(true)
                .oneTimeKeyboard(false)
                .keyboard(Arrays.asList(backToMainMenuKeyboardMarkup))
                .build();
    }

    @Override
    public List<SendMessage> tradeName(Update update, List<SendMessage> responseMessages, String chatId) {
        SendMessage message = SendMessage.builder()
                .text("Здійснюємо пошук препарату за його назвою. Для цього введіть назву препарату, котрого ви хочете побачити.\n\n Наприклад: Цитрамон")
                .chatId(chatId)
                .replyMarkup(backToMainMenuKeyboardMarkup)
                .build();

        userChoiseService.save(getFirstname(update), getLastname(update), getUsername(update), MessageChoiseEnum.TRADE_NAME);

        responseMessages.add(message);

        return responseMessages;
    }

    @Override
    public List<SendMessage> analogs(Update update, List<SendMessage> responseMessages, String chatId) {
        SendMessage message = SendMessage.builder()
                .text("Здійснюємо пошук аналогів препарату. Для цього введіть назву препарату, аналогів котрого ви хочете побачити.\n\nНаприклад: Амізон")
                .chatId(chatId)
                .replyMarkup(backToMainMenuKeyboardMarkup)
                .build();

        userChoiseService.save(getFirstname(update), getLastname(update), getUsername(update), MessageChoiseEnum.ANALOG);

        responseMessages.add(message);

        return responseMessages;
    }

    @Override
    public List<SendMessage> activeSubstance(Update update, List<SendMessage> responseMessages, String chatId) {
        SendMessage message = SendMessage.builder()
                .text("Здійснюємо пошук препарату за діючими речовинами. Для цього введіть діючу речовину препарату, котрого ви хочете побачити.\n\n Наприклад: Дутастерид")
                .chatId(chatId)
                .replyMarkup(backToMainMenuKeyboardMarkup)
                .build();

        userChoiseService.save(getFirstname(update), getLastname(update), getUsername(update), MessageChoiseEnum.SUBSTANCE);

        responseMessages.add(message);

        return responseMessages;
    }

    @Override
    public List<SendMessage> tradeName(List<SendMessage> responseMessages, String name, int position, String chatId) {
        List<Preparation> preparations = searchService.findByTradeName(name, position);
        return generateAnswer(responseMessages, preparations, position, name, chatId);
    }

    @Override
    public List<SendMessage> analogs(List<SendMessage> responseMessages, String name, int position, String chatId) {
        List<Preparation> preparations = searchService.findAnalogs(name, position);
        return generateAnswer(responseMessages, preparations, position, name, chatId);
    }

    @Override
    public List<SendMessage> activeSubstance(List<SendMessage> responseMessages, String name, int position, String chatId) {
        List<Preparation> preparations = searchService.findByActiveSubstance(name, position);
        return generateAnswer(responseMessages, preparations, position, name, chatId);
    }

    private List<SendMessage> generateAnswer(List<SendMessage> responseMessages, List<Preparation> preparations, int position, String name, String chatId) {
        if(Objects.isNull(preparations) || preparations.isEmpty()) {
            if(position > START_PAGINATION) {
                responseMessages.add(createMessage("Усі препарати знайдено. Якщо ви хочете продовжити пошук - натискайте кнопку \"Назад\"", chatId));
            } else {
                responseMessages.add(createMessage("Схоже нічого не знайдено. Перевірте запит та спробуйте знову.", chatId));
            }
        } else if(preparations.size() == ONE_ELEMENT) {
            responseMessages.add(createMessage(formMessage(preparations.get(FIRST_ELEM)), chatId));
            responseMessages.add(createMessage("Усі препарати знайдено. Якщо ви хочете продовжити пошук - натискайте кнопку \"Назад\"", chatId));
        } else if(preparations.size() < SearchService.PAGINATION_SIZE){
            responseMessages.addAll(formMessage(preparations, chatId));
            responseMessages.add(createMessage("Усі препарати знайдено. Якщо ви хочете продовжити пошук - натискайте кнопку \"Назад\"", chatId));
        } else {
            responseMessages.addAll(formMessage(preparations, SearchService.PAGINATION_SIZE, chatId));

            InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
            List<InlineKeyboardButton> rowInline = new ArrayList<>();

            InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
            inlineKeyboardButton.setText("Наступні 5 препаратів");
            inlineKeyboardButton.setCallbackData(MessageChoiseEnum.TRADE_NAME.getId() + SEPARATOR + name + SEPARATOR + (position + SearchService.PAGINATION_SIZE));

            rowInline.add(inlineKeyboardButton);
            rowsInline.add(rowInline);
            markupInline.setKeyboard(rowsInline);

            SendMessage message = createMessage("Продовжуємо пошук?", chatId);
            message.setReplyMarkup(markupInline);

            responseMessages.add(message);
        }

        return responseMessages;
    }

    public static String formMessage(Preparation preparation) {
        StringBuilder builder = new StringBuilder("")
                .append("<b>Назва:</b> ")                              .append(preparation.getTradeName())
                .append("\n<b>Форма випуску:</b> ")                    .append(preparation.getVacationConditions())
                .append("\n<b>Умови відпуску:</b> ")                   .append(Objects.isNull(preparation.getRealeseForm().getName()) ? "Звичайні" : preparation.getRealeseForm().getName())
                .append("\n<b>Склад (діючі):</b> ")                    .append(preparation.getDrugComposition())
                .append("\n<b>Фармакотерапевтична група:</b> ")        .append(preparation.getPharmacotherapeuticGroup().getName())
                .append("\n<b>Країна виробник:</b> ")                  .append(preparation.getCountry().getName())
                .append("\n<b>Виробник:</b> ")                         .append(preparation.getManufacture().getName())
                .append("\n<b>Адреса виробника:</b> ")                 .append(preparation.getAddressManufacture())
                .append("\n<b>Номер Реєстраційного посвідчення:</b> ") .append(preparation.getRegistrationCertificateNumber())
                .append("\n<b>Тип ЛЗ:</b> ")                           .append(preparation.getDrugType().getName())
                .append("\n<b>ЛЗ біологічного походження:</b> ")       .append(preparation.getBiologicalOrigin() ? "Так" : "Ні")
                .append("\n<b>ЛЗ рослинного походження:</b> ")         .append(preparation.getPlantOrigin() ? "Так" : "Ні")
                .append("\n<b>ЛЗ-сирота:</b> ")                        .append(preparation.getOrphan() ? "Так" : "Ні")
                .append("\n<b>Гомеопатичний ЛЗ:</b> ")                 .append(preparation.getHomeopathic() ? "Так" : "Ні")
                .append("\n<b>Термін придатності:</b> ")                .append(preparation.getExpirationDate()).append(" ").append(preparation.getExpirationDateType().getName());

        if(Objects.nonNull(preparation.getInstructionsURL()) && !preparation.getInstructionsURL().isEmpty()) {
            builder.append("\n<b>URL інструкції:</b> ").append(preparation.getInstructionsURL());
        }

        return builder.toString();
    }

    public static List<SendMessage> formMessage(List<Preparation> preparations, String chatId) {
        List<SendMessage> sendMessages = new ArrayList<>();

        preparations.forEach(p -> {
            sendMessages.add(createMessage(formMessage(p), chatId));
        });

        return sendMessages;
    }

    public static List<SendMessage> formMessage(List<Preparation> preparations, int paginationSize, String chatId) {
        return formMessage(preparations.subList(START_LIST, paginationSize), chatId);
    }

    public static String getUsername(Update update) {
        if(update.hasCallbackQuery()) {
            return update.getCallbackQuery().getFrom().getUserName();
        }

        return update.getMessage().getFrom().getUserName();
    }

    public static String getLastname(Update update) {
        if(update.hasCallbackQuery()) {
            return update.getCallbackQuery().getFrom().getLastName();
        }

        return update.getMessage().getFrom().getLastName();
    }

    public static String getFirstname(Update update) {
        if(update.hasCallbackQuery()) {
            return update.getCallbackQuery().getFrom().getFirstName();
        }

        return update.getMessage().getFrom().getFirstName();
    }

    private static SendMessage createMessage(String message, String chatId) {
        return SendMessage.builder()
                .chatId(chatId)
                .text(message)
                .parseMode("HTML")
                .build();
    }
}