package ua.donnu.edu.tabletbot.service;

import com.haulmont.cuba.core.global.DataManager;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Service;
import ua.donnu.edu.tabletbot.entity.Preparation;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service(SearchService.NAME)
public class SearchServiceBean implements SearchService {

    private static final int FIRST_ELEM         = 0 ;
    private static final int START_WORD         = 0 ;
    private static final int END_WORD           = 2 ;
    private static final int MAX_RESULT_RECORD  = PAGINATION_SIZE ;

    @Inject private DataManager dataManager;

    @Override
    public List<Preparation> findByTradeName(String name, int position) {
        List<Preparation> name1 = dataManager.load(Preparation.class)
                .query("select e from tabletbot_Preparation e where e.tradeName like :name")
                .firstResult(position)
                .maxResults(position + MAX_RESULT_RECORD)
                .parameter("name", "%" + name.toUpperCase() + "%")
                .view("preparation-view")
                .list();

        return name1;
    }

    @Override
    public List<Preparation> findAnalogs(String name, int position) {
        List<Preparation> name1 = findByTradeName(name, position);

        if(Objects.isNull(name1) || name1 .isEmpty()) {
            return new ArrayList<>();
        }

        return name1.get(FIRST_ELEM).getAnalogs();
    }

    @Override
    public List<Preparation> findByActiveSubstance(String name, int position) {
        if(name.length() > 3) {
            name = name.substring(START_WORD, name.length() - END_WORD);
        }

        return dataManager.load(Preparation.class)
                .query("select e from tabletbot_Preparation e where e.drugComposition like :name")
                .firstResult(position)
                .maxResults(MAX_RESULT_RECORD)
                .parameter("name", "%" + name + "%")
                .view("preparation-view")
                .list();
    }
}