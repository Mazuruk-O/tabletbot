package ua.donnu.edu.tabletbot.service;

import com.haulmont.cuba.core.global.DataManager;
import org.springframework.stereotype.Service;
import ua.donnu.edu.tabletbot.entity.ChoiseUser;
import ua.donnu.edu.tabletbot.entity.MessageChoiseEnum;
import ua.donnu.edu.tabletbot.entity.Preparation;

import javax.inject.Inject;
import java.util.Objects;

@Service(UserChoiseService.NAME)
public class UserChoiseServiceBean implements UserChoiseService {

    @Inject private DataManager dataManager;

    @Override
    public boolean save(String firstname, String lastname, String username, MessageChoiseEnum choise) {
        ChoiseUser choiseUser = dataManager.create(ChoiseUser.class);
        choiseUser.setUsername(getUniqueUsername(firstname, lastname, username));
        choiseUser.setChoise(choise);

        dataManager.commit(choiseUser);

        return true;
    }

    @Override
    public MessageChoiseEnum findLastChoise(String firstname, String lastname, String username) {
        ChoiseUser choise = dataManager.load(ChoiseUser.class)
                .query("select e from tabletbot_ChoiseUser e where e.username = :username order by e.createTs desc")
                .parameter("username", getUniqueUsername(firstname, lastname, username))
                .one();

        return choise.getChoise();
    }

    public String getUniqueUsername(String firstname, String lastname, String username) {
        StringBuilder usernameUnique = new StringBuilder("");

        if(Objects.nonNull(firstname) && !firstname.isEmpty()) {
            usernameUnique.append(firstname);
        }

        if(Objects.nonNull(lastname) && !lastname.isEmpty()) {
            usernameUnique.append(lastname);
        }

        if(Objects.nonNull(username) && !username.isEmpty()) {
            usernameUnique.append(username);
        }

        return usernameUnique.toString();
    }
}