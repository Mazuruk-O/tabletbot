package ua.donnu.edu.tabletbot.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TABLETBOT_CHOISE_USER")
@Entity(name = "tabletbot_ChoiseUser")
public class ChoiseUser extends StandardEntity {
    private static final long serialVersionUID = 2947934940705137085L;

    @Column(name = "USERNAME", length = 2048)
    private String username;

    @Column(name = "CHOISE")
    private String choise;

    public MessageChoiseEnum getChoise() {
        return choise == null ? null : MessageChoiseEnum.fromId(choise);
    }

    public void setChoise(MessageChoiseEnum choise) {
        this.choise = choise == null ? null : choise.getId();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}