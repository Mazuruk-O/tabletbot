package ua.donnu.edu.tabletbot.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TABLETBOT_DRUG_TYPE")
@Entity(name = "tabletbot_DrugType")
@NamePattern("%s|name")
public class DrugType extends StandardEntity {
    private static final long serialVersionUID = -8441519325802103636L;

    @Column(name = "NAME", length = 2048)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}