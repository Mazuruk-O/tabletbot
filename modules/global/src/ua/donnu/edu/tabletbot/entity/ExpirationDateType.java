package ua.donnu.edu.tabletbot.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Термін придатності: значення
 * Приклад можливих значень: рік, місяць, година
 */
@Table(name = "TABLETBOT_EXPIRATION_DATE_TYPE")
@Entity(name = "tabletbot_ExpirationDateType")
@NamePattern("%s|name")
public class ExpirationDateType extends StandardEntity {
    private static final long serialVersionUID = -1356798618882310024L;

    @Column(name = "NAME", length = 2048)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}