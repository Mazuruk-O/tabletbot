package ua.donnu.edu.tabletbot.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TABLETBOT_MANUFACTURE")
@Entity(name = "tabletbot_Manufacture")
@NamePattern("%s|name")
public class Manufacture extends StandardEntity {
    private static final long serialVersionUID = 2441285770538283884L;

    @Column(name = "NAME", length = 2048)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}