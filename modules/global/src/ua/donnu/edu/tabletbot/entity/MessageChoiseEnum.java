package ua.donnu.edu.tabletbot.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum MessageChoiseEnum implements EnumClass<String> {

    SEARCH("Пошук"),
    BACK("Назад"),
    ANALOG("Пошук аналогу"),
    SUBSTANCE("Пошук за діючою речовиною"),
    SYMPTOMS("Пошук за симптомами"),
    DISEASE("Пошук за захворюванням"),
    TRADE_NAME("Пошук за назвою"),
    MAIN("На головну");

    private String id;

    MessageChoiseEnum(String value) {
        this.id = value;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static MessageChoiseEnum fromId(String id) {
        for (MessageChoiseEnum at : MessageChoiseEnum.values()) {
            if (at.getId().equalsIgnoreCase(id)) {
                return at;
            }
        }
        return null;
    }
}