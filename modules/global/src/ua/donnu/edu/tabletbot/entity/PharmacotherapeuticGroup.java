package ua.donnu.edu.tabletbot.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TABLETBOT_PHARMACOTHERAPEUTIC_GROUP")
@Entity(name = "tabletbot_PharmacotherapeuticGroup")
@NamePattern("%s|name")
public class PharmacotherapeuticGroup extends StandardEntity {
    private static final long serialVersionUID = 6296169537304467112L;

    @Column(name = "NAME", length = 5096)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}