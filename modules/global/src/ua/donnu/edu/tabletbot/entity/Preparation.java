package ua.donnu.edu.tabletbot.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.util.List;

@Table(name = "TABLETBOT_PREPARATION")
@Entity(name = "tabletbot_Preparation")
@NamePattern("%s|tradeName")
public class Preparation extends StandardEntity {
    private static final long serialVersionUID = 9120699123908962478L;

    @Column(name = "ID_DRLZ", length = 64)
    private String idDRLZ;

    @Column(name = "TRADE_NAME", length = 2048)
    private String tradeName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REALESE_FORM_ID")
    private ReleaseForm realeseForm;

    @Column(name = "VACATION_CONDITIONS", length = 2048)
    private String vacationConditions;

    @Column(name = "DRUG_COMPOSITION", length = 5096)
    private String drugComposition;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PHARMACOTHERAPEUTIC_GROUP_ID")
    private PharmacotherapeuticGroup pharmacotherapeuticGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MANUFACTURE_ID")
    private Manufacture manufacture;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNTRY_ID")
    private Country country;

    @Column(name = "ADDRESS_MANUFACTURE", length = 2048)
    private String addressManufacture;

    @Column(name = "REGISTRATION_CERTIFICATE_NUMBER", length = 1024)
    private String registrationCertificateNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DRUG_TYPE_ID")
    private DrugType drugType;

    @Column(name = "BIOLOGICAL_ORIGIN")
    private Boolean biologicalOrigin;

    @Column(name = "PLANT_ORIGIN")
    private Boolean plantOrigin;

    @Column(name = "ORPHAN")
    private Boolean orphan;

    @Column(name = "HOMEOPATHIC")
    private Boolean homeopathic;

    @Column(name = "INSTRUCTIONS_URL", length = 2048)
    private String instructionsURL;

    @Column(name = "EXPIRATION_DATE")
    private Integer expirationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXPIRATION_DATE_TYPE_ID")
    private ExpirationDateType expirationDateType;

    @JoinTable(name = "TABLETBOT_PREPARATION_PREPARATION_LINK",
            joinColumns = @JoinColumn(name = "PREPARATION_1_ID"),
            inverseJoinColumns = @JoinColumn(name = "PREPARATION_2_ID"))
    @ManyToMany
    private List<Preparation> analogs;

    public List<Preparation> getAnalogs() {
        return analogs;
    }

    public void setAnalogs(List<Preparation> analogs) {
        this.analogs = analogs;
    }

    public PharmacotherapeuticGroup getPharmacotherapeuticGroup() {
        return pharmacotherapeuticGroup;
    }

    public void setPharmacotherapeuticGroup(PharmacotherapeuticGroup pharmacotherapeuticGroup) {
        this.pharmacotherapeuticGroup = pharmacotherapeuticGroup;
    }

    public ReleaseForm getRealeseForm() {
        return realeseForm;
    }

    public void setRealeseForm(ReleaseForm realeseForm) {
        this.realeseForm = realeseForm;
    }

    public String getIdDRLZ() {
        return idDRLZ;
    }

    public void setIdDRLZ(String idDRLZ) {
        this.idDRLZ = idDRLZ;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getVacationConditions() {
        return vacationConditions;
    }

    public void setVacationConditions(String vacationConditions) {
        this.vacationConditions = vacationConditions;
    }

    public String getDrugComposition() {
        return drugComposition;
    }

    public void setDrugComposition(String drugComposition) {
        this.drugComposition = drugComposition;
    }

    public Manufacture getManufacture() {
        return manufacture;
    }

    public void setManufacture(Manufacture manufacture) {
        this.manufacture = manufacture;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getAddressManufacture() {
        return addressManufacture;
    }

    public void setAddressManufacture(String addressManufacture) {
        this.addressManufacture = addressManufacture;
    }

    public String getRegistrationCertificateNumber() {
        return registrationCertificateNumber;
    }

    public void setRegistrationCertificateNumber(String registrationCertificateNumber) {
        this.registrationCertificateNumber = registrationCertificateNumber;
    }

    public DrugType getDrugType() {
        return drugType;
    }

    public void setDrugType(DrugType drugType) {
        this.drugType = drugType;
    }

    public Boolean getBiologicalOrigin() {
        return biologicalOrigin;
    }

    public void setBiologicalOrigin(Boolean biologicalOrigin) {
        this.biologicalOrigin = biologicalOrigin;
    }

    public Boolean getPlantOrigin() {
        return plantOrigin;
    }

    public void setPlantOrigin(Boolean plantOrigin) {
        this.plantOrigin = plantOrigin;
    }

    public Boolean getOrphan() {
        return orphan;
    }

    public void setOrphan(Boolean orphan) {
        this.orphan = orphan;
    }

    public Boolean getHomeopathic() {
        return homeopathic;
    }

    public void setHomeopathic(Boolean homeopathic) {
        this.homeopathic = homeopathic;
    }

    public String getInstructionsURL() {
        return instructionsURL;
    }

    public void setInstructionsURL(String instructionsURL) {
        this.instructionsURL = instructionsURL;
    }

    public ExpirationDateType getExpirationDateType() {
        return expirationDateType;
    }

    public void setExpirationDateType(ExpirationDateType expirationDateType) {
        this.expirationDateType = expirationDateType;
    }

    public Integer getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Integer expirationDate) {
        this.expirationDate = expirationDate;
    }
}