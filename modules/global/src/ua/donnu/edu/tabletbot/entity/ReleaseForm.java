package ua.donnu.edu.tabletbot.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TABLETBOT_RELEASE_FORM")
@Entity(name = "tabletbot_ReleaseForm")
@NamePattern("%s|name")
public class ReleaseForm extends StandardEntity {
    private static final long serialVersionUID = -3576196752254275750L;

    @Column(name = "NAME", length = 1024)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}