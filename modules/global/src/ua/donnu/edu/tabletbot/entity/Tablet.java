package ua.donnu.edu.tabletbot.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TABLETBOT_TABLET")
@Entity(name = "tabletbot_Tablet")
public class Tablet extends StandardEntity {
    private static final long serialVersionUID = -8383103065434472377L;
}