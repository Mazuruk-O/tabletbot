package ua.donnu.edu.tabletbot.service;

public interface BotInitializerService {
    String NAME = "tabletbot_BotInitializerService";

    void Init();
}