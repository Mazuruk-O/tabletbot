package ua.donnu.edu.tabletbot.service;

import org.telegram.telegrambots.meta.generics.LongPollingBot;

public interface BotService extends LongPollingBot {
    String NAME = "tabletbot_BotService";
}