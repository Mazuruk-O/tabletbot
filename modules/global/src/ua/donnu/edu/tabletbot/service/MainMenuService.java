package ua.donnu.edu.tabletbot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.List;

public interface MainMenuService {
    String NAME = "tabletbot_MainMenuService";
    void Init();
    List<SendMessage> main(List<SendMessage> responseMessages, String chatId);
    List<SendMessage> search(List<SendMessage> responseMessages, String chatId);
    List<SendMessage> help(List<SendMessage> responseMessages, String chatId);
}