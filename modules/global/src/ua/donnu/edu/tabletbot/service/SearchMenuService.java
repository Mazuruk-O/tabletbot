package ua.donnu.edu.tabletbot.service;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

public interface SearchMenuService {
    String NAME = "tabletbot_SearchMenuService";

    char SEPARATOR    = ';';

    void Init();
    List<SendMessage> tradeName(Update update, List<SendMessage> responseMessages, String chatId);
    List<SendMessage> tradeName(List<SendMessage> responseMessages, String name, int position, String chatId);
    List<SendMessage> analogs(Update update, List<SendMessage> responseMessages, String chatId);
    List<SendMessage> analogs(List<SendMessage> responseMessages, String name, int position, String chatId);
    List<SendMessage> activeSubstance(Update update, List<SendMessage> responseMessages, String chatId);
    List<SendMessage> activeSubstance(List<SendMessage> responseMessages, String name, int position, String chatId);
}