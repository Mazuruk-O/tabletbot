package ua.donnu.edu.tabletbot.service;

import ua.donnu.edu.tabletbot.entity.Preparation;

import java.util.List;

public interface SearchService {
    String NAME = "tabletbot_SearchService";

    static final int PAGINATION_SIZE =  5;

    List<Preparation> findByTradeName(String name, int position);
    List<Preparation> findAnalogs(String name, int position);
    List<Preparation> findByActiveSubstance(String name, int position);
}