package ua.donnu.edu.tabletbot.service;

import ua.donnu.edu.tabletbot.entity.MessageChoiseEnum;

public interface UserChoiseService {
    String NAME = "tabletbot_UserChoiseService";

    boolean save(String firstname, String lastname, String username, MessageChoiseEnum choise);
    MessageChoiseEnum findLastChoise(String firstname, String lastname, String username);
}